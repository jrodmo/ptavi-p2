#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


def plus(comp1, comp2):
    """ Funciópn para restar componentes. Números enteros """
    return comp1 + comp2


def minus(comp1, comp2):
    """ Función para restar componentes. Números enteros """
    return comp1 - comp2


if __name__ == "__main__":
    try:
        componente1 = int(sys.argv[1])
        componente2 = int(sys.argv[3])
    except ValueError:
        sys.exit("No es un numero")

    if sys.argv[2] == "suma":
        result = plus(componente1, componente2)
    elif sys.argv[2] == "resta":
        result = minus(componente1, componente2)
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
