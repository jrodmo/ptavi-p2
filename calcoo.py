#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys


class Calculadora ():
    def plus(comp1, comp2):
        """ Function to sum the operands. Ops have to be ints """
        return comp1 + comp2

    def minus(comp1, comp2):
        """ Function to substract the operands """
        return comp1 - comp2


if __name__ == "__main__":
    try:
        componente1 = int(sys.argv[1])
        componente2 = int(sys.argv[3])
    except ValueError:
        sys.exit("Error: Non numerical parameters")

    if sys.argv[2] == "suma":
        result = Calculadora.plus(componente1, componente2)
    elif sys.argv[2] == "resta":
        result = Calculadora.minus(componente1, componente2)
    else:
        sys.exit('Operación sólo puede ser sumar o restar.')

    print(result)
