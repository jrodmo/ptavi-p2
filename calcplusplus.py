import sys
import calcoohija
import csv

cont_op = 1

with open(sys.argv[1], 'r') as csvarchivo:
    fichero = csv.reader(csvarchivo, delimiter=",")
    list = list(fichero)

for conjunto in list:
    operaciones = conjunto[2:]
    if __name__ == "__main__":
        result = 0
        try:
            listsnum = [int(i) for i in conjunto[1:]]
            if conjunto[0] == 'suma':
                suma = 0
                for i in listsnum:
                    suma = calcoohija.CalculadoraHija.plus(i, suma)
                    result = suma
                print("Resultado de la suma:", result)
            elif conjunto[0] == 'resta':
                resta = listsnum[0] * 2
                for i in listsnum:
                    resta = calcoohija.CalculadoraHija.minus(resta, i)
                    result = resta
                print("Resultado de la resta:", result)
            elif conjunto[0] == 'multiplica':
                multiplica = 1
                for i in listsnum:
                    multiplica = calcoohija.CalculadoraHija.mult(multiplica, i)
                    result = multiplica
                print("Resultado de la multiplicacion:", result)
            elif conjunto[0] == 'divide':
                result = int(conjunto[1])
                for operando in operaciones:
                    cont_op += 1
                    componente1 = int(conjunto[cont_op])
                    try:
                        result = calcoohija.CalculadoraHija.div(result,
                                                                componente1)
                    except ZeroDivisionError:
                        sys.exit("Error: Division by zero is not allowed")
                print("Resultado de la división: ", result)
            else:
                result = 'Op. sólo puede ser suma,resta,mult. o divide.'

        except ValueError:
            result = "No son parámetros correctos"
            print(result)
