import sys
import calcoo


class CalculadoraHija(calcoo.Calculadora):
    def mult(comp1, comp2):
        """ Función para multiplicar los componentes """
        return comp1 * comp2

    def div(comp1, comp2):
        """ Función para dividir los componentes """
        return comp1 / comp2


if __name__ == "__main__":
    try:
        componente1 = int(sys.argv[1])
        componente2 = int(sys.argv[3])
    except ValueError:
        sys.exit("No es un número")

    if sys.argv[2] == "suma":
        result = CalculadoraHija.plus(componente1, componente2)
    elif sys.argv[2] == "resta":
        result = CalculadoraHija.minus(componente1, componente2)
    elif sys.argv[2] == "multiplica":
        result = CalculadoraHija.mult(componente1, componente2)
    elif sys.argv[2] == "divide":
        if sys.argv[3] != '0':
            result = CalculadoraHija.div(componente1, componente2)
        else:
            sys.exit("No se puede dividir por cero")
    else:
        sys.exit('La operación no es válida.')

    print(result)
